<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");

require_once('MysqliDb.php');


class API {
    public function __construct()
    {
        $this->db = new MysqliDB('localhost', 'root', '', 'employee');
    }

    public function validatePutId($id){
        $exploded_id = explode(",", $id);
        if(count($exploded_id) == 1){
            if((int)$exploded_id[0]){
                $this->db->where('id', (int)$exploded_id[0]);
                $query = $this->db->get('information');
                if(!$query){
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    public function validateDelId($id){

        if(is_array($id)){
            foreach($id as $ids){
                $this->db->where('id', $ids);
                $query = $this->db->get('information');
                if(!$query){
                    return false;
                }
                return true;
            }
        }
        else{
            $this->db->where('id', $id);
            $query = $this->db->get('information');
            if(!$query){
                return false;
            }
            return true;
        }
        
    }

    public function validatePostpayload($payload, $accepted_parameters){
        if(!empty($payload)){
            if (is_array($payload)) {
                $payload_param = array();
                foreach($payload as $param => $value){
                    array_push($payload_param, $param);
                }
                foreach ($accepted_parameters as $param) {
                    if(!in_array($param, $payload_param)){
                        return false;
                    }
                }
                foreach ($payload_param as $param) {
                    if(!in_array($param, $accepted_parameters)){
                        return false;
                    }
                }
                return true; // all properties are valid
            }
            return false; // not an array
        }
        return false; // empty
    }
    
    public function validatePutpayload($payload, $accepted_parameters){
        if(!empty($payload)){
            if (is_array($payload)) {
                $payload_param = array();
                foreach($payload as $param => $value){
                    array_push($payload_param, $param);
                }
                foreach ($accepted_parameters as $param) {
                    if(!in_array($param, $payload_param)){
                        return false;
                    }
                }
                foreach ($payload_param as $param) {
                    if(!in_array($param, $accepted_parameters)){
                        return false;
                    }
                }
                return true; // all properties are valid
            }
            return false; // not an array
        }
        return false; // empty
    }

    public function validateGetpayload($payload, $accepted_parameters){
        if(!empty($payload)){
            if (is_array($payload)) {
                $payload_param = array();
                foreach($payload as $param => $value){
                    array_push($payload_param, $param);
                }
                foreach ($accepted_parameters as $param) {
                    if(!in_array($param, $payload_param)){
                        return false;
                    }
                }
                
                return true; // all properties are valid
            }
            return false; // not an array
        }
        return false; // empty
    }

    /**
       * HTTP POST Request
       *
       * @param $payload
       */
    public function httpPost($payload)
    {
        //validateId
        $error = array();
        $accepted_parameter = array('first_name', 'middle_name', 'last_name', 'contact_number');
        if(array_key_exists("id",$payload)){
            unset($payload['id']);
        }
        
        if($this->validatePostpayload($payload, $accepted_parameter) == true){
            foreach($payload as $values){
                if(!empty($values)){
                    if(key($payload) != "contact_number"){
                        if(!is_string($values)){
                            array_push($error, key($payload));
                        }
                    }
                    else{
                        if(!is_int($values)){
                            array_push($error, key($payload));
                        }
                    }
                }
                else{
                    array_push($error, key($payload));
                }
                next($payload);
            }
        }
        else{
            array_push($error, "parameters");
        }

        if(!empty($error)){
            return json_encode(array(
                'method' => 'POST',
                'status' => 'fail',
                'data' => $error,
                'message' => 'Failed to insert, Invalid Data'
            ));
        }
        else{
            $query = $this->db->insert('information',$payload);
            if($query){
                return json_encode(array(
                    'method' => 'POST',
                    'status' => 'success',
                    'data' => $payload,
                ));
            }
            else{
                return json_encode(array(
                    'method' => 'POST',
                    'status' => 'fail',
                    'data' => $error,
                    'message' => 'Failed to insert'
                ));
            }
        }
        
    }

    /**
       * HTTP GET Request
       *
       * @param $payload
    */
    public function httpGet($payload)
    {
        // execute query
        $error = array();
        $accepted_parameter = array('id');
        
        if($this->validateGetpayload($payload, $accepted_parameter) == true){
            if(!empty($payload['id'])){
                if(is_array($payload['id'])){
                    $count = 0;
                    foreach($payload['id'] as $ids){
                        if(!is_int($ids)){
                            $count++;
                        }
                    }
                    if($count === 0){
                        $this->db->where('id', $payload['id'], 'IN');
                        $query = $this->db->get('information');
                    }
                    else{
                        array_push($error, "ID");
                    }
                }
                else{
                    if(!is_int($payload['id'])){
                        array_push($error, "ID");
                    }
                    else{
                        $this->db->where('id', $payload['id']);
                        $query = $this->db->getOne('information');
                    }
                }
            }
            else{
                array_push($error, "ID");
            }
        }
        else{
            array_push($error, "paramaters");
        }

        if(empty($error)){
            
            if ($query) {
                return json_encode(array(
                    'method' => 'GET',
                    'status' => 'success',
                    'data' => $query
                ));
            }
            else{
                return json_encode(array(
                    'method' => 'GET',
                    'status' => 'fail',
                    'data' => [],
                    'message' => 'Not found'
                ));
            }
        }
        else{
            return json_encode(array(
                'method' => 'GET',
                'status' => 'fail',
                'data' => $error,
                'message' => 'Invalid Data'
            ));
        }
    }

    /**
       * HTTP PUT Request
       *
       * @param $id
       * @param $payload
       */
    public function httpPut($id, $payload)
    {
        $error = array();
        $selected_id = explode(",", $id);
        $payload_param = array('id', 'first_name', 'middle_name', 'last_name', 'contact_number');
        
        if($this->validatePutpayload($payload, $payload_param) == true){
            if($this->validatePutId($id) == true && $this->validatePutId($payload['id']) == true){
                if($selected_id[0] == $payload['id']){
                    foreach($payload as $values){
                        if(!empty($values)){
                            if(key($payload) != "id" && key($payload) != "contact_number"){
                                if(!is_string($values)){
                                    array_push($error, key($payload));
                                }
                            }
                            else{
                                if(!is_int($values)){
                                    array_push($error, key($payload));
                                }
                            }
                        }
                        else{
                            array_push($error, key($payload));
                        }
                        next($payload);
                    }
                }
                else{
                    array_push($error, "id mismatch");
                }
            }
            else{
                array_push($error, "id");
            }
        }
        else{
            array_push($error, "parameters");
        }

        if(empty($error)){
            $this->db->where('id',(int)$selected_id[0]);
            $query = $this->db->update('information', $payload);
            if ($query) {
                return json_encode(array(
                    'method' => 'PUT',
                    'status' => 'success',
                    'data' => $payload,
                ));
            } else {
                return json_encode(array(
                    'method' => 'PUT',
                    'status' => 'fail',
                    'data' => [],
                    'message' => 'Failed to Update'
                ));
            }
        }
        else{
            return json_encode(array(
                'method' => 'PUT',
                'status' => 'fail',
                'data' => $error,
                'message' => 'Invalid Data'
            ));
        }
    }

    /**
       * HTTP DELETE Request
       *
       * @param $payload
       */

    public function httpDelete($ids, $payload)
    {
        if(is_string($ids)){
            
            $error = array();
            $selected_id = explode(",", $ids);
            if($this->validateDelId($selected_id) == true){
                if(count($selected_id)){
                    if(array_key_exists('id', $payload)){
                        if(is_array($payload['id'])){
                            if(!array_diff($selected_id,$payload['id'])){
                                $this->db->where('id', $selected_id, 'IN');
                            }
                            else{
                                array_push($error, "ID Mismatch");
                            }
                        }
                        else{
                            $id_array = array();
                            array_push($id_array,$payload['id']);
                            if(!array_diff($selected_id,$id_array)){
                                $this->db->where('id', $selected_id, 'IN');
                            }
                            else{
                                array_push($error, "ID Mismatch");
                            }
                        }
                    }
                    else{
                        array_push($error, "Missing ID");
                    }
                }
                else{
                    array_push($error, "Missing ID");
                }
            }
            else{
                array_push($error, "ID not found");
            }

            if(empty($error)){
                $query = $this->db->delete("information");
                if ($query) {
                    return json_encode(array(
                        'method' => 'DELETE',
                        'status' => 'success',
                        'data' => $payload,
                    ));
                    return;
                }
                else{
                    return json_encode(array(
                        'method' => 'DELETE',
                        'status' => 'fail',
                        'data' => [],
                        'message' => 'Failed to delete'
                    ));
                }
            }
            else{
                return json_encode(array(
                    'method' => 'DELETE',
                    'status' => 'fail',
                    'data' => $error,
                    'message' => 'Invalid Data'
                ));
            }
        }
    }
}
    /*
    if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
        if ($_SERVER['REQUEST_METHOD'] === 'PUT' || $_SERVER['REQUEST_METHOD'] === 'DELETE') {
            $exploded_uri = explode('/', $_SERVER['REQUEST_URI']);
            $last_index = count($exploded_uri) - 1;
            $ids = $exploded_uri[$last_index];
        } 
    } else {
        $received_data = $_GET;
    }
    */
    $request_method = $_SERVER['REQUEST_METHOD'];
    
    //payload data
    $receive_data = json_decode(file_get_contents('php://input'),true);
    $api = new API;

    switch($request_method) {
        case 'GET':
            $api->httpGet($receive_data);
            break;
        case 'POST':
            $api->httpPost($receive_data);
            break;
        case 'PUT':
            $api->httpPut($ids, $receive_data);
            break;
        case 'DELETE':
            $api->httpDelete($ids, $receive_data);
            break;
    }
    
?>