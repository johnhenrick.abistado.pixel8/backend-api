<?php

require_once('api.php');
use PHPUnit\Framework\TestCase;

class APITest extends TestCase
{
    protected function setUp(): void{
        $this->api = new API();
    }

    public function testPayload(): array{

        $payload = array(
            'id' => 1,
            'first_name' => 'John Henrick',
            'middle_name' => 'Mosende',
            'last_name' => 'Abistado',
            'contact_number' => 9829898
        );
        $this->assertTrue(true);
        return $payload;
    }

    /**
     * @depends testPayload 
     */
    public function testHttPost(array $payload){
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $result = json_decode($this->api->httpPost($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

    /**
     * @depends testPayload 
     */
    public function testHttpGet(array $payload){
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $result = json_decode($this->api->httpGet($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

    /**
     * @depends testPayload 
     */
    public function testHttpPut(array $payload){
        $ids = strval($payload['id']);
        $_SERVER['REQUEST_METHOD'] = 'PUT';
        $result = json_decode($this->api->httpPut($ids,$payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

    /**
     * @depends testPayload 
     */
    public function testHttpDelete(array $payload){
        $ids = strval($payload['id']);
        $_SERVER['REQUEST_METHOD'] = 'DELETE';
        $result = json_decode($this->api->httpDelete($ids,$payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }
}

?>